class Vacancy < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order('created_at DESC') }
  validates :start_at, presence: true
  validates :vacanctseats, presence: true
  validates :vehicle, presence: true, length: { maximum: 20 }
  validates :user_id, presence: true
end
