class Request < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order('created_at DESC') }
  validates :start_at, presence: true
  validates :destination, presence: true
  validates :group_size, presence: true
  validates :user_id, presence: true
end
