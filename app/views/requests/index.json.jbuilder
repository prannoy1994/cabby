json.array!(@requests) do |request|
  json.extract! request, :start_at, :destination, :group_size, :comments, :user_id
  json.url request_url(request, format: :json)
end
