json.array!(@vacancies) do |vacancy|
  json.extract! vacancy, :start_at, :vehicle, :vacanctseats, :comments, :user_id
  json.url vacancy_url(vacancy, format: :json)
end
