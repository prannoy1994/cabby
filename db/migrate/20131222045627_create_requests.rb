class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.datetime :start_at
      t.string :destination
      t.integer :group_size
      t.text :comments
      t.integer :user_id

      t.timestamps
    end
  add_index :vacancies, [:user_id, :created_at]
  end
end
