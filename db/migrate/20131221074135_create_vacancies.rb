class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.datetime :start_at
      t.string :vehicle
      t.integer :vacanctseats
      t.text :comments
      t.integer :user_id

      t.timestamps
    end
  add_index :vacancies, [:user_id, :created_at]
  end
end
